import com.scharfmedia.grails.plugins.home.HomeHelper

class HomeGrailsPlugin {
    // the plugin version
    def version = "0.2"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.0 > *"
    // the other plugins this plugin depends on
    def dependsOn = [:]
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def title = "Home Plugin" // Headline display name of the plugin
    def author = "Christoph Scharf"
    def authorEmail = "christoph.scharf@scharfmedia.de"
    def description = '''\
Separate your runtime configuration and data from code.

This Plugin allows you to have a separate directory where your configuration files and runtime data is located.
For Example, you can change your Grails Config without redeploying your Application.

The application Home directory is per default in your users Home directory located beneath the Grails application name
(the name you have used with your "grails create-app $appName$") command.
You can also look it up or modify it in your application.properties (app.name).

The path to your application Home can as well be specified as System-Environment paramenter $APPNAME$_HOME=/path/to/your/home or as Java parameter with -D$appName$.home.location=/path/to/your/home
or a config entry in Config.groovy with "grails.plugins.home.location".

You can also override the directory structure which gets generated per default.
This can be done by overriding the config entry "grails.plugins.home.scaffolding".
For example:
    grails.plugins.home.scaffolding {
	    directories = ["config","data"]
		files = ["config/Config.groovy"]
	}
'''

    // URL to the plugin's documentation
    def documentation = "https://bitbucket.org/scharfmedia/grails-plugin-home/src"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
    def organization = [ name: "scharfmedia GmbH", url: "http://www.scharfmedia.de" ]

    // Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

    // Location of the plugin's issue tracker.
//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPMYPLUGIN" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/scharfmedia/grails-plugin-home/src" ]

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before
    }

    def doWithSpring = {
        // TODO Implement runtime spring config (optional)

        // initialize Home config files
        HomeHelper.initConfig( application )
        // initialize Home Directory based on new config
        // => creating files and directories
        HomeHelper.initHome( application )
        // merge live config with external config files
		HomeHelper.refreshConfig( application )
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.

        // hot reload external configs.
        // note: this is something you propably never want to have in your production environment as
        // watching resources and dynamically reloading them costs especially memory and performance.
        HomeHelper.initConfig( application )
    }

    def onShutdown = { event ->
        // TODO Implement code that is executed when the application shuts down (optional)
    }

}

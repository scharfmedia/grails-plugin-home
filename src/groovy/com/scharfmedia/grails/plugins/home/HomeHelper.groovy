package com.scharfmedia.grails.plugins.home

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.commons.cfg.ConfigurationHelper


/**
 * HomeHelper
 * to do all the magic behind the curtain
 */
class HomeHelper {

    /**
     * init external config files from application Home directory
     * @param application the grailsApplication instance
     */
    public static void initConfig( GrailsApplication application ) {
        def userHome = System.getProperty("user.home")
        def appName  = application.metadata['app.name']

        // initialize config home entry
/*
    note: this does for some reason not work with resources plugin, as it will throw
          a can not access null on fixtablib message...
		if( !application.config.grails.plugins )
			application.config.grails.plugins = [:]
*/
		if( !application.config.grails.plugins.home )
        	application.config.grails.plugins.home = [:] 

		// get shortcut to plugin home config section
        def home = application.config.grails.plugins.home

        // set application home location to either java environment property "$appname$.home.location" or system environment property "$APPNAME$_HOME" or config
        // entry "grails.plugins.home.location" or "userhome/appname"
        home.location = System.properties["${appName}.home.location"] ?: System.env["${appName}_HOME".toUpperCase().replaceAll('-','')] ?: home.location ?: "${userHome}/${appName}"

        // scaffolding template for structure to create
		if( !home.scaffolding ) {
	        home.scaffolding = [
				directories: [
			        "conf",
			        "tmp",
			        "db"
				],
				files: [
			        "conf/Config.groovy",
			        "conf/DataSource.groovy"
		        ]
			]
		}
	    // end - of scaffolding template

        // add home configs to this config
        application.config.grails.config.locations = [
                "file:${home.location}/conf/Config.groovy",
                "file:${home.location}/conf/DataSource.groovy"
        ]
    }

    /**
     * initialize application Home directory
     * - creates folder structure
     *   and
     * - file templates
     *  @param config the initialized Grails config
     **/
    public static void initHome( GrailsApplication application ) {
        // get home directory from "in-source"-Config.groovy
        def home        = application.config.grails.plugins.home.location
		def scaffolding = application.config.grails.plugins.home.scaffolding

        // create scaffolding structure
        scaffolding.directories.each { dir -> new File("${home}/${dir}").mkdirs() }
        scaffolding.files.each { file -> def f = new File("${home}/${file}"); if(!f.exists()) f.createNewFile()  }
    }

	/**
	 * merges new configs with existing application configs
	 **/
	public static void refreshConfig( GrailsApplication application ) {
		ConfigurationHelper.initConfig( application.config )
	}
}

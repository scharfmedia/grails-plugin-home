# INSTALL
grails install-plugin grails-plugin-home-0.2.zip
or
copy the grails-plugin-home-0.2.jar
into your lib directory

# CHECKOUT
run grails upgrade after freh checkout

# ABOUT
Separate your runtime configuration and data from code.

This Plugin allows you to have a separate directory where your configuration files and runtime data is located.
For Example, you can change your Grails Config without redeploying your Application.

The application Home directory is per default in your users Home directory located beneath the Grails application name
(the name you have used with your "grails create-app $appName$") command.
You can also look it up or modify it in your application.properties (app.name).

The path to your application Home can as well be specified as System-Environment paramenter $APPNAME$_HOME=/path/to/your/home or as Java parameter with -D$appName$.home.location=/path/to/your/home
or a config entry in Config.groovy with "grails.plugins.home.location".

You can also override the directory structure which gets generated per default.
This can be done by overriding the config entry "grails.plugins.home.scaffolding".
For example:
    grails.plugins.home.scaffolding {
	    directories = ["config","data"]
		files = ["config/Config.groovy"]
	}

